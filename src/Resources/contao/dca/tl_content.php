<?php

/**
 * 361GRAD Element Flipcardteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_flipcardtripleteaser'] =
    '{type_legend},type,headline;' .
    '{text_legend},text;' .
    '{first_teaser_legend},first_teaser_headline,first_teaser_text,first_teaser_backtext,first_teaser_link,first_teaser_linktext,first_teaser_target;' .
    '{second_teaser_legend},second_teaser_headline,second_teaser_text,second_teaser_backtext,second_teaser_link,second_teaser_linktext,second_teaser_target;' .
    '{third_teaser_legend},third_teaser_headline,third_teaser_text,third_teaser_backtext,third_teaser_link,third_teaser_linktext,third_teaser_target;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['fields']['first_teaser_headline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['first_teaser_headline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h1',
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['first_teaser_text']        = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['first_teaser_text'],
    'inputType' => 'textarea',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr',
        'rte'       => 'tinyMCE',
    ],
    'sql'       => 'text NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['first_teaser_backtext']        = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['first_teaser_backtext'],
    'inputType' => 'textarea',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr',
        'rte'       => 'tinyMCE',
    ],
    'sql'       => 'text NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['first_teaser_link'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['first_teaser_link'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['first_teaser_target'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['first_teaser_target'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['first_teaser_linktext'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['first_teaser_linktext'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['second_teaser_headline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['second_teaser_headline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h1',
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['second_teaser_text']        = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['second_teaser_text'],
    'inputType' => 'textarea',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr',
        'rte'       => 'tinyMCE',
    ],
    'sql'       => 'text NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['second_teaser_backtext']        = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['second_teaser_backtext'],
    'inputType' => 'textarea',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr',
        'rte'       => 'tinyMCE',
    ],
    'sql'       => 'text NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['second_teaser_link'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['second_teaser_link'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['second_teaser_target'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['second_teaser_target'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['second_teaser_linktext'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['second_teaser_linktext'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['third_teaser_headline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['third_teaser_headline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h1',
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['third_teaser_text']        = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['third_teaser_text'],
    'inputType' => 'textarea',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr',
        'rte'       => 'tinyMCE',
    ],
    'sql'       => 'text NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['third_teaser_backtext']        = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['third_teaser_backtext'],
    'inputType' => 'textarea',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr',
        'rte'       => 'tinyMCE',
    ],
    'sql'       => 'text NULL'
];
$GLOBALS['TL_DCA']['tl_content']['fields']['third_teaser_link'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['third_teaser_link'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'clr w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['third_teaser_target'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['third_teaser_target'],
    'inputType' => 'checkbox',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'clr w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['third_teaser_linktext'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['third_teaser_linktext'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop']    = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];