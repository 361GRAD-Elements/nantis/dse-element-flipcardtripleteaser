<?php

/**
 * 361GRAD Element Flipcardtripleteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_flipcardtripleteaser'] = ['Flipcard (Triple) Teaser', 'Teaser with flipcard effect on hover.'];

$GLOBALS['TL_LANG']['tl_content']['first_teaser_legend']   = 'First Teaser';
$GLOBALS['TL_LANG']['tl_content']['first_teaser_headline']   = ['Headline', ''];
$GLOBALS['TL_LANG']['tl_content']['first_teaser_text']   = ['Text', 'Front card text'];
$GLOBALS['TL_LANG']['tl_content']['first_teaser_backtext']   = ['Text', 'Back card text'];
$GLOBALS['TL_LANG']['tl_content']['first_teaser_link']   = ['Link', 'Please enter a web address (http://…), an e-mail address (mailto:…) or an insert'];
$GLOBALS['TL_LANG']['tl_content']['first_teaser_linktext']   = ['Link text', 'Text of the link'];
$GLOBALS['TL_LANG']['tl_content']['first_teaser_target']   = ['Link Target', 'Open the link in a new browser window.'];

$GLOBALS['TL_LANG']['tl_content']['second_teaser_legend']   = 'Second Teaser';
$GLOBALS['TL_LANG']['tl_content']['second_teaser_headline']   = ['Headline', ''];
$GLOBALS['TL_LANG']['tl_content']['second_teaser_text']   = ['Text', 'Front card text'];
$GLOBALS['TL_LANG']['tl_content']['second_teaser_backtext']   = ['Text', 'Back card text'];
$GLOBALS['TL_LANG']['tl_content']['second_teaser_link']   = ['Link', 'Please enter a web address (http://…), an e-mail address (mailto:…) or an insert'];
$GLOBALS['TL_LANG']['tl_content']['second_teaser_linktext']   = ['Link text', 'Text of the link'];
$GLOBALS['TL_LANG']['tl_content']['second_teaser_target']   = ['Link Target', 'Open the link in a new browser window.'];

$GLOBALS['TL_LANG']['tl_content']['third_teaser_legend']   = 'Third Teaser';
$GLOBALS['TL_LANG']['tl_content']['third_teaser_headline']   = ['Headline', ''];
$GLOBALS['TL_LANG']['tl_content']['third_teaser_text']   = ['Text', 'Front card text'];
$GLOBALS['TL_LANG']['tl_content']['third_teaser_backtext']   = ['Text', 'Back card text'];
$GLOBALS['TL_LANG']['tl_content']['third_teaser_link']   = ['Link', 'Please enter a web address (http://…), an e-mail address (mailto:…) or an insert'];
$GLOBALS['TL_LANG']['tl_content']['third_teaser_linktext']   = ['Link text', 'Text of the link'];
$GLOBALS['TL_LANG']['tl_content']['third_teaser_target']   = ['Link Target', 'Open the link in a new browser window.'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];
