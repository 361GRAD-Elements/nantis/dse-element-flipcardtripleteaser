<?php

/**
 * 361GRAD Element Flipcardtripleteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_flipcardtripleteaser'] = ['Flipcard (Dreifach) Teaser', 'Teaser mit Flipcard-Effekt auf Schwebeflug.'];

$GLOBALS['TL_LANG']['tl_content']['first_teaser_legend']   = 'Erster Teaser';
$GLOBALS['TL_LANG']['tl_content']['first_teaser_headline']   = ['Überschrift', ''];
$GLOBALS['TL_LANG']['tl_content']['first_teaser_text']   = ['Text', 'Vorderer Kartentext'];
$GLOBALS['TL_LANG']['tl_content']['first_teaser_backtext']   = ['Text', 'Zurück Kartentext'];
$GLOBALS['TL_LANG']['tl_content']['first_teaser_link']   = ['Link', 'Bitte geben Sie eine Internetadresse (http: // ...), eine E-Mail-Adresse (mailto: ...) oder eine Einfügung ein'];
$GLOBALS['TL_LANG']['tl_content']['first_teaser_linktext']   = ['Link text', 'Text des Links'];
$GLOBALS['TL_LANG']['tl_content']['first_teaser_target']   = ['Linkziel', 'Öffnen Sie den Link in einem neuen Browserfenster.'];

$GLOBALS['TL_LANG']['tl_content']['second_teaser_legend']   = 'Zweite Teaser';
$GLOBALS['TL_LANG']['tl_content']['second_teaser_headline']   = ['Überschrift', ''];
$GLOBALS['TL_LANG']['tl_content']['second_teaser_text']   = ['Text', 'Vorderer Kartentext'];
$GLOBALS['TL_LANG']['tl_content']['second_teaser_backtext']   = ['Text', 'Zurück Kartentext'];
$GLOBALS['TL_LANG']['tl_content']['second_teaser_link']   = ['Link', 'Bitte geben Sie eine Internetadresse (http: // ...), eine E-Mail-Adresse (mailto: ...) oder eine Einfügung ein'];
$GLOBALS['TL_LANG']['tl_content']['second_teaser_linktext']   = ['Link text', 'Text des Links'];
$GLOBALS['TL_LANG']['tl_content']['second_teaser_target']   = ['Linkziel', 'Öffnen Sie den Link in einem neuen Browserfenster.'];

$GLOBALS['TL_LANG']['tl_content']['third_teaser_legend']   = 'Dritte Teaser';
$GLOBALS['TL_LANG']['tl_content']['third_teaser_headline']   = ['Überschrift', ''];
$GLOBALS['TL_LANG']['tl_content']['third_teaser_text']   = ['Text', 'Vorderer Kartentext'];
$GLOBALS['TL_LANG']['tl_content']['third_teaser_backtext']   = ['Text', 'Zurück Kartentext'];
$GLOBALS['TL_LANG']['tl_content']['third_teaser_link']   = ['Link', 'Bitte geben Sie eine Internetadresse (http: // ...), eine E-Mail-Adresse (mailto: ...) oder eine Einfügung ein'];
$GLOBALS['TL_LANG']['tl_content']['third_teaser_linktext']   = ['Link text', 'Text des Links'];
$GLOBALS['TL_LANG']['tl_content']['third_teaser_target']   = ['Linkziel', 'Öffnen Sie den Link in einem neuen Browserfenster.'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];
