<?php

/**
 * 361GRAD Flipcardtripleteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementFlipcardtripleteaser;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the 361GRAD Flipcardtripleteaser 
 */
class DseElementFlipcardtripleteaser extends Bundle
{
}
