<?php

namespace Dse\ElementsBundle\ElementFlipcardtripleteaser\ContaoManager;

use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Dse\ElementsBundle\ElementFlipcardtripleteaser;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ElementFlipcardtripleteaser\DseElementFlipcardtripleteaser::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
